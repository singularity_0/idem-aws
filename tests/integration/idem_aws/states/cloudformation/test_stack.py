import copy
import json
import pathlib
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-cloudformation-stack" + str(int(time.time())),
    "disable_rollback": False,
    "enable_termination_protection": False,
}
tags = [{"Key": "Stage", "Value": "QA"}]
template = str(pathlib.Path(__file__).parent / "create_stack_design_template")
parameters_in_ret_format = {}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_ec2_key_pair, cleanup):
    global PARAMETER
    template = str(pathlib.Path(__file__).parent / "create_stack_design_template")

    key_pair_name = aws_ec2_key_pair.get("name")

    to_dump = {
        "AWSTemplateFormatVersion": "2010-09-09",
        "Parameters": {
            "KeyName": {"Type": "String", "Default": key_pair_name},
            "SSHLocation": {"Type": "String", "Default": "0.0.0.0/0"},
            "InstanceType": {"Type": "String", "Default": "t2.small"},
        },
        "Resources": {
            "EC2SGOFPN": {
                "Type": "AWS::EC2::SecurityGroup",
                "Properties": {"GroupDescription": "testing"},
                "Metadata": {
                    "AWS::CloudFormation::Designer": {
                        "id": "2bb7c2d6-7682-4d85-8c58-483f2c7d04d2"
                    }
                },
            }
        },
    }

    parameters_in_ret_format["param"] = [
        {"ParameterKey": "KeyName", "ParameterValue": key_pair_name},
        {"ParameterKey": "SSHLocation", "ParameterValue": "0.0.0.0/0"},
        {"ParameterKey": "InstanceType", "ParameterValue": "t2.small"},
    ]

    f = open(template)
    with open(template, "w") as file:
        json.dump(fp=file, indent=2, obj=to_dump)
    loaded = json.load(f)
    PARAMETER["template_body"] = str(loaded)

    ctx["test"] = __test
    present_ret = await hub.states.aws.cloudformation.stack.present(
        ctx,
        **PARAMETER,
    )
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret.get("new_state")
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.cloudformation.stack", name=PARAMETER["name"]
            )[0]
            in present_ret["comment"]
        )
        assert PARAMETER["disable_rollback"] == resource.get("disable_rollback")
        assert PARAMETER["enable_termination_protection"] == resource.get(
            "enable_termination_protection"
        )
    else:
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.cloudformation.stack", name=PARAMETER["name"]
            )[0]
            in present_ret["comment"]
        )
        PARAMETER["resource_id"] = resource.get("resource_id")
        assert parameters_in_ret_format["param"] == resource.get("parameters")
        assert resource["stack_status"] == "CREATE_COMPLETE"


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.cloudformation.stack.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.cloudformation.stack.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.cloudformation.stack.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")
    assert PARAMETER["name"] == described_resource_map.get("name")
    assert parameters_in_ret_format["param"] == described_resource_map.get("parameters")
    assert described_resource_map.get("stack_status") == "CREATE_COMPLETE"


@pytest.mark.skip(reason="no way of testing this currently")
async def test_update_parameters(hub, ctx):
    global PARAMETER
    # The dependency that this test uses in the Cloudformation Stack (EC2 with Security Group) doesn't support
    # updating parameters by passing parameters directly as part of update call. However, writing updated parameters to
    # the template body don't reliably alter the parameters. Therefore, skipping this test currently.


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update_tests", depends=["describe"])
async def test_updating(hub, ctx, __test, aws_ec2_key_pair):
    global PARAMETER
    ctx["test"] = "--test"
    new_parameter = copy.deepcopy(PARAMETER)

    if __test:
        new_parameter["disable_rollback"] = True
        new_parameter["enable_termination_protection"] = True
        new_parameter["parameters"] = [
            {"ParameterKey": "KeyName", "ParameterValue": aws_ec2_key_pair.get("name")},
            {"ParameterKey": "SSHLocation", "ParameterValue": "0.0.0.0/0"},
            {"ParameterKey": "InstanceType", "ParameterValue": "t2.micro"},
        ]

        ret = await hub.states.aws.cloudformation.stack.present(ctx, **new_parameter)
        resource = ret["new_state"]
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.cloudformation.stack", name=new_parameter["name"]
            )[0]
            in ret["comment"]
        )
        assert new_parameter["name"] == resource["name"]
        assert new_parameter["resource_id"] == resource["resource_id"]
        assert new_parameter["parameters"] == resource["parameters"]
        assert resource.get("stack_status") == "CREATE_COMPLETE"
        assert new_parameter["disable_rollback"] == resource["disable_rollback"]
        assert new_parameter["template_body"] == resource["template_body"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update_tags", depends=["update-tests"])
async def test_update_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["tags"] = [{"Key": "Stage", "Value": "QA"}]

    ret = await hub.states.aws.cloudformation.stack.present(ctx, **new_parameter)
    resource = ret["new_state"]
    assert PARAMETER["name"] == resource["name"]
    assert PARAMETER["resource_id"] == resource["resource_id"]
    assert resource["stack_status"] == "CREATE_COMPLETE"
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.cloudformation.stack", name=new_parameter["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.cloudformation.stack", name=new_parameter["name"]
            )[0]
            in ret["comment"]
        )
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        assert parameters_in_ret_format["param"] == resource.get("parameters")

        if not __test:
            PARAMETER = new_parameter


@pytest.mark.skip(reason="no way of testing this currently")
async def test_remove_tags(hub, ctx):
    global PARAMETER
    # Currently, not possible to remove tags for Stacks, both in CLI and Amazon web interface. Removing tags thus not
    # currently supported in this Idem Resource.


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get", depends=["update_tags"])
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.cloudformation.stack.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert resource.get("stack_status") == "UPDATE_COMPLETE"
    assert parameters_in_ret_format["param"] == resource.get("parameters")

    if not hub.tool.utils.is_running_localstack(ctx):
        assert tags == resource.get("tags")


@pytest.mark.skip(reason="no way of testing this currently")
async def test_remove_tags(hub, ctx):
    global PARAMETER
    # Currently, not possible to remove tags for Stacks, both in CLI and Amazon web interface. Removing tags thus not
    # currently supported in this Idem Resource.


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get", depends=["update_tags"])
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.cloudformation.stack.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert resource.get("stack_status") == "UPDATE_COMPLETE"
    assert parameters_in_ret_format["param"] == resource.get("parameters")

    if not hub.tool.utils.is_running_localstack(ctx):
        assert tags == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["exec-get"])
async def test_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.cloudformation.stack.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert resource.get("stack_status") == "UPDATE_COMPLETE"
    assert parameters_in_ret_format["param"] == resource.get("parameters")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.cloudformation.stack",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.cloudformation.stack",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )

    if not hub.tool.utils.is_running_localstack(ctx):
        assert tags == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.cloudformation.stack.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"]
    assert ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.cloudformation.stack",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_stack_absent_with_none_resource_id(hub, ctx, __test):
    ctx["test"] = __test
    "idem-cloudformation-stack-" + str(int(time.time()))
    # Delete CloudformationStack with resource_id as None. Result in no-op.
    ret = await hub.states.aws.cloudformation.stack.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.cloudformation.stack",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.cloudformation.stack.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
