import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_public_access_block(hub, ctx, aws_s3_bucket):
    temp_name = "idem-test-bucket-public-access-block-" + str(uuid.uuid4())
    bucket_name = aws_s3_bucket.get("name")
    public_access_block_configuration_to_add = {
        "BlockPublicAcls": True,
        "IgnorePublicAcls": True,
        "BlockPublicPolicy": True,
        "RestrictPublicBuckets": True,
    }
    public_access_block_configuration_to_update = {
        "BlockPublicAcls": False,
        "IgnorePublicAcls": False,
        "BlockPublicPolicy": False,
        "RestrictPublicBuckets": False,
    }
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # create public-access-block with test flag
    ret = await hub.states.aws.s3.public_access_block.present(
        test_ctx,
        name=temp_name,
        bucket=bucket_name,
        public_access_block_configuration=public_access_block_configuration_to_add,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.s3.public_access_block '{temp_name}' configuration for bucket {bucket_name}"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert temp_name == resource.get("name")
    assert public_access_block_configuration_to_add == resource.get(
        "public_access_block_configuration"
    )

    # create public-access-block in real
    ret = await hub.states.aws.s3.public_access_block.present(
        ctx,
        name=temp_name,
        bucket=bucket_name,
        public_access_block_configuration=public_access_block_configuration_to_add,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created aws.s3.public_access_block '{temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    resource_name = resource.get("name")
    assert public_access_block_configuration_to_add == resource.get(
        "public_access_block_configuration"
    )

    # describe public-access-block
    describe_ret = await hub.states.aws.s3.public_access_block.describe(ctx)
    assert describe_ret
    assert "aws.s3.public_access_block.present" in describe_ret.get(resource_name)
    described_resource = describe_ret.get(resource_name).get(
        "aws.s3.public_access_block.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "bucket" in described_resource_map
    assert "resource_id" in described_resource_map
    assert bucket_name == described_resource_map["resource_id"]
    assert bucket_name == described_resource_map["bucket"]
    assert "public_access_block_configuration" in described_resource_map
    assert (
        public_access_block_configuration_to_add
        == described_resource_map["public_access_block_configuration"]
    )

    # Update public-access-block with test flag
    ret = await hub.states.aws.s3.public_access_block.present(
        test_ctx,
        name=temp_name,
        bucket=bucket_name,
        public_access_block_configuration=public_access_block_configuration_to_update,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would update aws.s3.public_access_block '{temp_name}' configuration for bucket {bucket_name}"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert public_access_block_configuration_to_update == resource.get(
        "public_access_block_configuration"
    )

    # update public-access-block in real
    ret = await hub.states.aws.s3.public_access_block.present(
        ctx,
        name=temp_name,
        bucket=bucket_name,
        public_access_block_configuration=public_access_block_configuration_to_update,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated aws.s3.public_access_block '{temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert public_access_block_configuration_to_update == resource.get(
        "public_access_block_configuration"
    )

    # Delete public-access-block with test flag
    ret = await hub.states.aws.s3.public_access_block.absent(
        test_ctx, name=resource_name, resource_id=bucket_name
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert ret["old_state"]["name"] == resource_name
    assert ret["old_state"]["bucket"] == bucket_name
    assert ret["old_state"]["public_access_block_configuration"] == resource.get(
        "public_access_block_configuration"
    )
    assert (
        f"Would delete aws.s3.public_access_block '{resource_name}' configuration for bucket {bucket_name}"
        in ret["comment"]
    )

    # Delete public-access-block in real
    ret = await hub.states.aws.s3.public_access_block.absent(
        ctx, name=resource_name, resource_id=bucket_name
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert ret["old_state"]["name"] == resource_name
    assert ret["old_state"]["bucket"] == bucket_name
    assert ret["old_state"]["public_access_block_configuration"] == resource.get(
        "public_access_block_configuration"
    )
    assert (
        f"Deleted aws.s3.public_access_block '{resource_name}' for bucket '{bucket_name}'"
        in ret["comment"]
    )

    # Deleting public-access-block again should be a no-op
    ret = await hub.states.aws.s3.public_access_block.absent(
        ctx, name=resource_name, resource_id=bucket_name
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.s3.public_access_block '{resource_name}' already absent" in ret["comment"]
    )
