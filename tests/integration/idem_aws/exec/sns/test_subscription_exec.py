import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_sns_subscription):
    subscription_get_name = "idem-test-exec-get-subscription-" + str(int(time.time()))
    ret = await hub.exec.aws.sns.subscription.get(
        ctx,
        name=subscription_get_name,
        resource_id=aws_sns_subscription["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_sns_subscription["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_topic_arn(hub, ctx, aws_sns_subscription):
    subscription_get_name = "idem-test-exec-get-subscription-" + str(int(time.time()))
    ret = await hub.exec.aws.sns.subscription.get(
        ctx,
        name=subscription_get_name,
        topic_arn=aws_sns_subscription["topic_arn"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_sns_subscription["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    subscription_get_name = "idem-test-exec-get-subscription-" + str(int(time.time()))
    ret = await hub.exec.aws.sns.subscription.get(
        ctx,
        name=subscription_get_name,
        resource_id="@Fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.sns.subscription '{subscription_get_name}' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_sns_subscription):
    subscription_list_name = "idem-test-exec-list-subscription-" + str(int(time.time()))
    ret = await hub.exec.aws.sns.subscription.list(ctx, name=subscription_list_name)
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    resource_id_list = []
    for subscription in ret["ret"]:
        resource_id_list.append(subscription["resource_id"])
    assert aws_sns_subscription["resource_id"] in resource_id_list
